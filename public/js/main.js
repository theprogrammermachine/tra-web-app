 

function checkSizes() {
    let j = 0;
    for (let i = 0; i < 24; i++) {
        let buildingItem = document.getElementById(`building-item${i}`);
        let rowStart = 0, rowEnd = 0, colStart = 0, colEnd = 0;
        if (i % 3 == 0) {
            rowStart = 0 + j * 2;
            rowEnd = 1 + j * 2;
            colStart = 1;
            colEnd = 2;
        }
        else if (i % 3 == 1) {
            rowStart = 0 + j * 2;
            rowEnd = 1 + j * 2;
            colStart = 2;
            colEnd = 3;
        }
        else if (i % 3 == 2) {
            rowStart = 1 + j * 2;
            rowEnd = 2 + j * 2;
            colStart = 1;
            colEnd = 3;
            j++;
        }
        buildingItem.style.gridRowStart = rowStart;
        buildingItem.style.gridRowEnd = rowEnd;
        buildingItem.style.gridColumnStart = colStart;
        buildingItem.style.gridColumnEnd = colEnd;
    }
}

window.addEventListener('resize', () => checkSizes());

let lastScrollTop = 0;
let appbarCollapsed = false;

function onLoginBtnClicked() {
    let homeContent = document.getElementById('home-content');
    for (let i = 0; i < 24; i++) {
        homeContent.innerHTML += `
            <div id="building-item${i}" class="${'building-card'}">
              
            <div>`;
    }
    checkSizes();
    const appbar = document.getElementById('home-appbar');
    const bottomBar = document.getElementById('home-bottom-bar');
    const searchbar = document.getElementById('searchbar');
    window.addEventListener("scroll", function(){ // or window.addEventListener("scroll"....
        var st = window.pageYOffset || document.documentElement.scrollTop; // Credits: "https://github.com/qeremy/so/blob/master/so.dom.js#L426"
        if (st > lastScrollTop){
            bottomBar.style.transform = 'translate(0, +100%)';
            bottomBar.style.transition = 'transform';
            bottomBar.style.transitionDuration = '.25s';
            appbar.style.transform = 'scaleY(.55)';
            appbar.style.transition = 'transform';
            appbar.style.transitionDuration = '.25s';
            searchbar.style.transform = 'translate(-50%, 0) scaleX(.75)';
            searchbar.style.transition = 'transform';
            searchbar.style.transitionDuration = '.25s';
            appbarCollapsed = true;
        } else {
            bottomBar.style.transform = 'translate(0, 0)';
            bottomBar.style.transition = 'transform';
            bottomBar.style.transitionDuration = '.25s';
            appbar.style.transform = 'scaleY(1)';
            appbar.style.transition = 'transform';
            appbar.style.transitionDuration = '.25s';
            if (searchbar === document.activeElement) {
                searchbar.style.transform = 'translate(-50%, 0) scaleX(1.25)';
            } else {
                searchbar.style.transform = 'translate(-50%, 0) scaleX(1)';
            }
            searchbar.style.transition = 'transform';
            searchbar.style.transitionDuration = '.25s';
            appbarCollapsed = false;
        }
        lastScrollTop = st <= 0 ? 0 : st; // For Mobile or negative scrolling
    }, false);
    searchbar.addEventListener('focus', function() {
        appbar.style.transform = 'scaleY(1)';
        appbar.style.transition = 'transform';
        appbar.style.transitionDuration = '.25s';
        searchbar.style.transform = 'translate(-50%, 0) scaleX(1.25)';
        searchbar.style.transition = 'transform';
        searchbar.style.transitionDuration = '.25s';
    });
    searchbar.addEventListener('focusout', function() {
        if (appbarCollapsed) {
            searchbar.style.transform = 'translate(-50%, 0) scaleX(.75)';
        } else {
            searchbar.style.transform = 'translate(-50%, 0) scaleX(1)';
        }
        searchbar.style.transition = 'transform';
        searchbar.style.transitionDuration = '.25s';
    });
    bottomBar.style.animation = 'bottomBarUp 1s';
    bottomBar.style.animationFillMode = 'forwards';
    let loginCard = document.getElementById('login-card');
    loginCard.style.animation = 'fadeOut 1s';
    loginCard.style.animationFillMode = "forwards";
    document.body.style.animation = 'changeBodyColor 1s';
    document.body.style.animationFillMode = 'forwards';
    homeContent.style.animation = 'slideInBuildings 1s';
    homeContent.style.animationFillMode = "forwards";
    appbar.style.animation = 'slideIn 1s';
    appbar.style.animationFillMode = 'forwards';
    setTimeout(() => {
        document.body.style.overflow = 'auto';
    }, 1050);
}

function onNavOpenBtnClicked() {
    document.getElementById("home-sidenav").style.right = "0";
}
  
function onNavCloseBtnClicked() {
    document.getElementById("home-sidenav").style.right = "-250px";
}